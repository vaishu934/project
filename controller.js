var app = angular.module("mainApp", []);
app.controller("people", function ($scope, $http) {
  //making a requeat to json file
  $http
    .get("http://10.21.96.69:8000/blogproject/myblog")
    .then(function (response) {
      //arr = [];
      //arr = response.data;
      console.log(response);
      //$scope.message = "";
      $scope.persons = response.data;
    });

  $scope.name = null;
  $scope.email = null;
  $scope.blogname = null;
  $scope.abstract = null;
  $scope.content = null;
  $scope.myFunc = function (name, email, blogname, abstract, content) {
    // var data = {
    //   name: name,
    //   email: email,
    //   blogname: blogname,
    //   abstract: abstract,
    //   content: content,
    // };
    var data = {};
    data["name"] = name;
    data["email"] = email;
    data["blogname"] = blogname;
    data["abstract"] = abstract;
    data["content"] = content;
    console.log(name, email, blogname, abstract, content);
    $http
      .post("http://10.21.96.69:8000/blogproject/myblog", JSON.stringify(data))

      .then(function (response) {
        console.log(response);
      });
  };
  $scope.delete = function (id) {
    console.log(id);
    $http
      .post(
        "http://10.21.96.69:8000/blogproject/delrecord",
        JSON.stringify({
          id: id,
        })
      )
      .then(function (response) {
        $http
          .get("http://10.21.96.69:8000/blogproject/myblog")
          .then(function (response) {
            console.log(response);
            $scope.persons = response.data;
          });
      });
  };
});
